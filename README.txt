CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Contact

INTRODUCTION
------------

Term Status is a module that will provide an option in terms edit form to
enable/disable taxonomy terms just status options on node edit form.
This module provides basic getter, setter function to get the status of a term
 or update any term's status.

REQUIREMENTS
------------
PHP 5.2 or higher.
Drupal 6.x.

INSTALLATION
------------

Following steps are needed to install Term Status:

1. Login as Admin user.
2. Go to 'admin/build/modules' and enable 'Term Status' module.
3. Click 'Save Configurations' to complete the installation process.

CONTACT
-------
http://drupal.org/user/659188/dashboard
Developer Contact: asimfastian@gmail.com
